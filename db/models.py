from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import Column, Integer, String, Float
from ourico.db                  import client_engine

Base = declarative_base()


class Recurso (Base):
    __tablename__ = 'recurso'

    id           = Column(Integer, primary_key=True)
    name         = Column(String)
    description  = Column(String)

class Docente(Base):
    __tablename__='docente'

    id      = Column(Integer, primary_key = True)
    nome    = Column(String)
    apelido = Column(String)
    faculdade = Column(String)
    email_docente = Column(String)
    password_app_docente = Column(String)

class Aluno(Base):
    __tablename__='aluno'
    id      = Column(Integer, primary_key = True)
    nome    = Column(String)
    apelido = Column(String)
    faculdade = Column(String)
    email = Column(String)
    password_app_aluno = Column(String)

class Reservoir (Base):
    __tablename__ = 'reservoir'

    id           = Column(Integer, primary_key=True)
    name         = Column(String)
    description  = Column(String)
    ilha         = Column(String)
    concelho     = Column(String)
    cidade       = Column(String)
    height       = Column(Float) # meters
    width        = Column(Float) # meters
    length       = Column(Float) # meters
    base64_image = Column(String)
    nr_inputs    = Column(Integer)
    nr_outputs   = Column(Integer)




Base.metadata.create_all(client_engine)
